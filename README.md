日志系统导入助手工具
===============

## 准备条件

* 运行环境php7.0+

## 安装

~~~
git clone https://gitee.com/dreamplay/common_log_transform.git
~~~

## 配置

```
[common_log] #自定义日志系统
#日志类型，nginx_log-nginx日志
TYPE=nginx_log
#当前服务器ip，记录作用，以便区分来源服务器
SERVER_ADDR=172.20.0.4
#项目名称，必填，区分项目
APP_NAME=test
#目标日志系统配置
COMMON_LOG_HOST=http://api.dreamplay.top:9006
COMMON_LOG_APPID=admin
COMMON_LOG_APPSECRET=linzhou
#是否启用kafka对列导入，需要安装kafka扩展
USE_KAFKA=false
KAFKA_BROKERS=172.31.18.23:9092

[nginx_log]
#nginx日志路径
PATH=/home/wwwlogs
```

## 运行

```
php think debug
```