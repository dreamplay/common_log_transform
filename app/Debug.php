<?php
declare (strict_types=1);

namespace app;

use app\tool\LaravelLogTool;
use app\tool\NginxLogTool;
use app\tool\ThinkphpLogTool;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\facade\Cache;

class Debug extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('debug')->setDescription('调试专用命令');
    }

    protected function execute(Input $input, Output $output)
    {
        try {
            Cache::set('last_start_time', time()); //记录一下，防止下面使用缓存出现问题
            $type = config('app.common_log.type');
            switch ($type) {
                case 'nginx':
                    NginxLogTool::work();
                    break;
                case 'thinkphp':
                    ThinkphpLogTool::work();
                    break;
                case 'laravel':
                    LaravelLogTool::work();
                    break;
            }
        } catch (\Throwable $e) {
            dump($e->getTraceAsString());
        }
    }

}
